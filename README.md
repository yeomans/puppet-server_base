#server_base

server_base is a Puppet module intended for base configuration of a server.


## Class server_base

### Parameters:

* `hosts`: A hash of host resource data.  This hash is passed to create_resources.

* `packages`: A hash of package resources.  This hash is passed to create_resources.  This 
    is intended for packages such as gcc that do not require other configuration and thus do not really
    need to be wrapped into a class.  The package resource parameter ensure is set to the 
    default value present. 
    
    Hiera is configured in common.yaml to to a deep merge of server_base::packages items.
    This allows you to specify a list of packages in common.yaml, and augment the list 
    in other .yaml files.


## Class server_base::root

Provides a place to set ssh keys and authorized keys for login.

### Parameters:

* `home` = '/root', 

* `key_name` = 'id_rsa': the name of the ssh private key file.  The public key file will be named 
    $key_name.pub .

* `private_key`: the ssh private key for user root.

* `public_key`: the ssh public key for user root.

* 'ssh_authorized_keys` = {}: A hash of ssh_authorized_key resources, to be passed to create_resources.
    The ssh_authorized_key parameters ensure and user are set to "present" and "root" respectively;
    you probably should not override the user value.

### Hiera example

All parameter values can be provided using hiera.


```yaml
server_base::hosts:
    www-dev.example.com:
        ensure: present
        ip: 192.168.65.47
        host_aliases: www-dev
    db-dev.example.com:
        ensure: present
        ip: 192.168.65.14
        
server_base::packages:
    git: {}
    "libxml2-dev": {ensure: absent}
    
server_base::root::ssh_authorized_keys:
    "joe_root":         
        key: "AAAAB3NzaC1yc2EAAAADAQABAAABAQCgwsurTRXpRZz6UpLFPcibH0BwJgR5uV2WbmYMO4rzxLiPaFB1lJtJmvXe2Yc5JSTkNnZKE+mSrGG+PLOGCsvXYE3lf8u63j1mp0WQNymQK8ZsUDlohRxmWbfjGrYrF5VFJmwTSyYCBekn2r8lbmcMj9WLbrFvDlu8GDQ61Xam8JJLXkSzeKZ0fnYz5X3j4FxafjjmKT2UYZre/r2509vfESNwCpVEeH80nM3ie6/dVJ6m3Ndopjl+Rn52qSYMZJ9CgI9WeVkPlbUhss5TCAgpB4D6dT0JP7+qm06P8Q07VsKovspX5drn+ug+JVXXEJsh0pLi26ggAE1kRkprhFmz"
        type: "ssh-rsa"

server_base::root::private_key: |
    -----BEGIN RSA PRIVATE KEY-----
    MIIEpAIBAAKCAQEAy1vpJMW0BpScYOHjTLD08CA2pg2n6PT0COnC/nl/wyoC3dJ4
    cEtg78BYT6NrpvuEjNDSQUhtVWDxfM5la4F5wLgO3eIoi3LzGPNWxK/QiJVBeBka
    pFDG8d81BstXWd0TPTRGkoeGRYNxh/BxHuq8YLdozl8iptUqIqYOoynJRlo+yCey
    xOtdi1ZVv18KoFF58AKl8S9Axdu/W/iZc7E9Yuz903wnXfwWaSYEYG4hgOJzfC6I
    4v8Cxu1KStA9O5jdf2m83alonL1uUmEg7ugBPi3HQJHXfb6V5uXbalmkSJ2kKmvm
    8p7lg0mpnbTunhXjunM2XGbKinKRq3vrn6aA3wIDAQABAoIBAElC0Vk4CEUeoL+e
    YT6tuXAQit+Dp+wa4xea726x4r8ykVnMiysZXNDCoI4AN7YugnAggr6WEx1hEV5/
    FLaILI6rQF/bK/Qg80cX0ziEhkHOU4mqsZhkqWdVnErYUp3DMT0pags727mLQwlQ
    K9CRjE/jg1RlnZwwyz80R62sTiCTmYl2hvakbRHDEUSy93Zh+x4NdeHUc2CCTn2P
    JjdgU2D2cHoWnPkRxwS3nprOglKDhMyShYpI9oDRa7MBC3CGA8mINWFKH/5aV47s
    GDqeMcVKY5aMX7zhf+H3psrwInrv5ytihh+UHKo0SzyAUZf5vZhXkORamoWb6f+Q
    LTz0BVECgYEA9pmoWgPTh6OF/sgzlPi+IGyQ/PkhyK+zl1ftfLVnw9Slpvh69Lfu
    F/ZSY/pidv2kLTYqXQNVcsca4Ihbkg53c5R+7lE26vFbQzukvngjos5rN/naLJr/
    Hg79W5HzhaABK01S1cTWQRQ/gQEy7DgT1rBbxdZSC3+/lfugoiiBh8UCgYEA0xxN
    a+lp2qcUmF9Hx4Z3ryDaumPp703kgfg6hQvDpN6w+f3R9G+M4EzN/uDyvKcMUWtC
    D6xp3OvDccHMDqzSblMjjGmgFQJL1T41KWEewDOPBl00udtiDzctu9JWqgVsBmoD
    3SHJ+/QzioOH3Q1t1WolkLTMLSNOxxVB1ICFTFMCgYEA7e9AsumyNdOfvD9rFbgT
    qrFWC+BNqpdtDv/10zgtUJRi6QIjkO/POSugP3sdenYs7LqwkWJgFgjqc+7de8Wd
    l9WVy1htRq9uQSbRRD4jFDla1n2q9xLuSRBpgOsILsw9lIxc+ombuNtSkqSXf7hz
    29f6FJ9ANnwiXRKW4R94K4kCgYBB8SpB1eQJt3zYZ5KtQVRrMLEB3am55TPGYTou
    StFu01ymieEmIB36BiOmRK+tu+Q+HmnBKZLbvg0LoIRT5SnJ6lBFUMz/Ivt6/XZv
    mXWYhcJcM4r/rwY/SHYRoA8QBqeKjVztmttTxyC+WH18z/AJ5KEdRUwtoUS/LOWm
    MgrxHwKBgQCCVaDWgY3uLJvRiqQUJxqoXgxcdBoFA+hz4wwIEmA7Pqf6Tvl0dEx+
    G5Az3TzSDs81u81yIULpocYNLNf1O3V7NeIWOnzDSFXZI+yA3RNISqqeDgvghrtd
    CzrwyV+/yMfjZL98wqanYM4VxYkut3BDb2vKJuQtnOrl/6NiqkJDWQ==
        -----END RSA PRIVATE KEY-----

server_base::root::public_key:
    "AAAAB3NzaC1yc2EAAAADAQABAAABAQDLW+kkxbQGlJxg4eNMsPTwIDamDafo9PQI6cL+eX/DKgLd0nhwS2DvwFhPo2um+4SM0NJBSG1VYPF8zmVrgXnAuA7d4iiLcvMY81bEr9CIlUF4GRqkUMbx3zUGy1dZ3RM9NEaSh4ZFg3GH8HEe6rxgt2jOXyKm1Soipg6jKclGWj7IJ7LE612LVlW/XwqgUXnwAqXxL0DF279b+JlzsT1i7P3TfCdd/BZpJgRgbiGA4nN8Loji/wLG7UpK0D07mN1/abzdqWicvW5SYSDu6AE+LcdAkdd9vpXm5dtqWaRInaQqa+bynuWDSamdtO6eFeO6czZcZsqKcpGre+ufpoDf test.example.com"    
```
