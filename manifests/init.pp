class server_base
    (
    Hash $hosts, 
    # packages that need to be installed, but aren't used by puppet classes.
    Hash $packages={}, 
    # packages that puppet classes need to realize.
    Hash $virtual_packages={}, 
    Hash $users={})
    {
    create_resources(host, $hosts)
    create_resources(server_base::user, $users)
    create_resources('package', $packages, {ensure => present}) 
    create_resources('@package', $virtual_packages, {ensure => present}) 
    include server_base::root
    }
