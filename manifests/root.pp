class server_base::root($home='/root', $key_name='id_rsa', $private_key, $public_key, $ssh_authorized_keys={}, $ssh_known_hosts={})
    {
    $ssh_dir = "$home/.ssh"
    file
        {
        "$ssh_dir":
        ensure => directory, 
        owner => 'root', 
        group => 'root',
        mode => '0700',
        }

    file
        {
        "$ssh_dir/$key_name":
        ensure => file, 
        owner => 'root', 
        group => 'root',
        mode => '0600',
        content => "$private_key"
        }
        
    file
        {
        "$ssh_dir/$key_name.pub":
        ensure => file, 
        owner => 'root', 
        group => 'root',
        mode => '0600',
        content => "$public_key"
        }
        
    create_resources(ssh_authorized_key, $ssh_authorized_keys, {"ensure"=>"present", "user"=>"root"})
    create_resources(sshkey, $ssh_known_hosts, {"ensure"=>"present", "target"=>"$ssh_dir/known_hosts"})
    }
