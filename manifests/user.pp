define server_base::user
    (
    Enum['present', 'absent'] $ensure,
    String $home,
    Boolean $managehome=true,
    # a password string encrypted using mkpasswd -m sha-512.
    Optional[String] $password=undef, 
    # user may need multiple ssh keys for login from different machines. Hash is 
    # ssh_authorized_key resource data, with 
    Hash $ssh_authorized_keys={},
    )
    {
    user
        {
        "$title":
        ensure => $ensure, 
        home => $home, 
        managehome => $managehome,
        password => $password,
        }
    
    create_resources(ssh_authorized_key, $ssh_authorized_keys, $defaults={'user' => $title})
    }